/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.directmedia.onlinestore.controller;

import com.directmedia.onlinestore.core.entity.Artist;
import com.directmedia.onlinestore.core.entity.Catalogue;
import com.directmedia.onlinestore.core.entity.Work;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Iterator;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author st3oto
 */
@WebServlet(name = "CatalogueServlet", urlPatterns = {"/catalogue"})
public class CatalogueServlet extends HttpServlet {

   protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	
       if(Catalogue.listOfWorks.isEmpty()) {
	
        Artist a = new Artist("Roy Lichtenstein");

        Artist b = new Artist("Pierre-Auguste");

        Artist c = new Artist("Gustave Klim");

       

        /*Création d'oeuvres */

        Work titre1 = new Work("Bal du Moulin de la Galette");

        Work titre2 = new Work("In the Car");

        Work titre3 = new Work("La toile d'or");

       

        /* Création de bean Work*/

        titre1.setMainArtist(a);

        titre2.setMainArtist(b);

        titre3.setMainArtist(c);

       

        titre1.setRelease(1976);

        titre2.setRelease(1963);

        titre3.setRelease(1907);

       

        titre1.setGenre("Peinture");

        titre2.setGenre("Litterature");

        titre3.setGenre("Peinture");

        titre1.setSummary("Bal du Moulin de la Galette est une peinture de 1876 par l'artiste français Pierre-Auguste Renoir. Elle loge au musée d'Orsay à Paris et est l'un des chefs-d'oeuvre les plus célèbres de l'impressionnisme.");

        titre2.setSummary("In the Car est un tableau Pop art réalisé en 1963 par l'artiste Pop art Roy Lichtenstein. La plus petite et plus vieille des deux versions de ce tableau a déjà détenu le record du prix le plus élevé pour une peinture de Lichtenstein.");

        titre3.setSummary("Le Baiser est une oeuvre du peintre symboliste Gustav Klimt, peinte entre 1907 et 1908, durant sa période dorée.");

        titre1.aa = "amine";
        titre2.aa = "nabil";
        titre3.aa = "brahim";

        /*Création de la bean Catalogue */

        Catalogue.listOfWorks.add(titre1);

        Catalogue.listOfWorks.add(titre2);

        Catalogue.listOfWorks.add(titre3);
        
		}
		
        PrintWriter out = response.getWriter();
		
        out.println("<html><body>"
                + "<p>La liste des Oeuvres :</p>"
                + "<ul>");     
  for (Iterator<Work> it = Catalogue.listOfWorks.iterator(); it.hasNext();) {
        Work oeuvre = it.next();
        out.println("<li><a href=\"work-details?id=\">" +oeuvre.getTitle()+ ", ("+oeuvre.getRelease()+ ")</a></li>");
        out.println(oeuvre.aa);
  }
        
        out.println("</ul></body></html>");  
		
	}

}