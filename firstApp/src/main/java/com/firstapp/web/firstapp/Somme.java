/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.firstapp.web.firstapp;

import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author st3oto
 */
@WebServlet(name = "Somme", urlPatterns = {"/somme"})
public class Somme extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
       resp.setContentType("text/html");
       int somme;
       int nombre1 = Integer.parseInt(req.getParameter("nombre1"));
       int nombre2 = Integer.parseInt(req.getParameter("nombre2"));
       somme = nombre1+nombre2;
       PrintWriter out = resp.getWriter();
       out.print("<html><body><p>La somme des deux nombres fournis en parametres est : "+somme+"</body></p></html>");
    }
    
}
