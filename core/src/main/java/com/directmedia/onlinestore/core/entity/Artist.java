/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.directmedia.onlinestore.core.entity;

/**
 *
 * @author st3oto
 */
public class Artist {
   

    /* Constructeur sans paramètre */



    public Artist() {

    }



    /*Constructeur avec paramètre name*/



    public Artist(String name) {

        this.name = name;

    }

   

    /* Déclaration des attributs*/

    private String name;

   

   

    /* Accesseurs et mutateurs  publiques*/



    public String getName() {

        return name;

    }



    public void setName(String name) {

        this.name = name;

    }   

}

