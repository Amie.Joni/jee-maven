/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package backofficecom.directmedia.onlinestore.backoffice.controller;

import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author st3oto
 */
@WebServlet(name="Home",urlPatterns= {"/home"})
public class HomeServlet extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		PrintWriter out = response.getWriter();
		out.println("<!DOCTYPE html>\n" + 
				"<html>\n" + 
				"<head>\n" + 
				"<meta charset=\"UTF-8\">\n" + 
				"<title>OnlineStore – Gestion de la boutique</title>\n" + 
				"</head>\n" + 
				"<body>\n" + 
				"<p>OnlineStore – Gestion de la boutique</p>\n" + 
				"<p><a href=\"catalogue\" >Accès au catalogue des oeuvres</a></p>\n" + 
				"</body>\n" + 
				"</html>");
	}

}
